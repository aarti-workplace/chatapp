from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import InputRequired, Length, EqualTo, ValidationError
from models import User
from passlib.hash import pbkdf2_sha256

""" To Check Login CRedential """
def invalidate_credential(form, field):
    login_form_username = form.username.data
    login_form_password = field.data

    user_obj = User.query.filter_by(username = login_form_username).first()

    #Check username is valid 
    if user_obj is None:
        raise ValidationError("Username or Password is incorrect")
    elif not pbkdf2_sha256.verify(login_form_password, user_obj.password):
        raise ValidationError("Username or Password may incorrect") 




class SignUpForm(FlaskForm):

    username = StringField('username_label',validators=[InputRequired(message='User Name is required'),])

    password = PasswordField('password_label',validators=[InputRequired(message='Password is Required'),
    Length(min=6,message='Password must be at least 6 characters long.')])
    confirm_password = PasswordField('confirm_pwd_label',validators=[InputRequired(message='This field is required'), EqualTo('password',message="Password is not Matched!")])

    submit = SubmitField('Sign Up')

    # To Check for duplicate user
    def validate_username(self, username):
        user_obj = User.query.filter_by(username = username.data).first()
        if user_obj:
            raise ValidationError("Username already exist!")

class SignInForm(FlaskForm):
    username = StringField('username_label',validators=[InputRequired(),])
    password = PasswordField('password_label',validators=[InputRequired(), invalidate_credential] )
    
    submit = SubmitField('Sign In')




