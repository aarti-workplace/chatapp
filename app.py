from time import localtime, strftime
from flask import Flask, render_template, redirect, url_for, flash
from wtf import *
from models import *
from flask_login import LoginManager, login_user, current_user, login_required, logout_user
from flask_socketio import SocketIO, send, emit, join_room, leave_room

# from flask_mysqldb import MySQL
# import MySQLdb.cursors


# Configuration of app
app = Flask(__name__)
app.config['SECRET_KEY'] = 'key'

#Instaintiate socketio 
socketio = SocketIO(app)

#CONFIGURATION OF DATABASE
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:asdfgh@localhost/chatapp'

# instance of sqlalchemy
# SQLAlchemy is a tool to access various SQL database
mysql = SQLAlchemy(app)

#Congigure Flask Login
loginData = LoginManager(app)
loginData.init_app(app)

ROOMS = ['College Group', 'School Group', 'Company Group']

@loginData.user_loader
def load_user(id):
   return User.query.get(int(id))

@app.route("/", methods=['GET','POST'])
def index():
    
    #Accessing SignUp Class
     formData = SignUpForm()
     if formData.validate_on_submit():
        #return "You Registered Successfully!!!"
         username = formData.username.data
         password = formData.password.data

        # Secure the password
         hash_password = pbkdf2_sha256.hash(password)

        #Add user to DB
         user = User(username = username, password = hash_password)
         mysql.session.add(user)
         mysql.session.commit()
         flash("Registered Successfully, now you can login!", "success")
         return redirect(url_for('login'))
     return render_template("index.html", form = formData )

@app.route('/login', methods = ['GET','POST'])
def login():

    loginData = SignInForm()

    # redirect validation on success
    if loginData.validate_on_submit():
        user_obj = User.query.filter_by(username = loginData.username.data).first()
        login_user(user_obj)
        return redirect(url_for('chat'))
    # if user used GET for login page
    return render_template('login.html',form = loginData)

# Chat Routing 
@app.route('/chat', methods= ['GET', 'POST'])
def chat():
    
    if not current_user.is_authenticated:
         flash("Please Login Before Acessing Chat Page", "danger")
         return redirect(url_for('login'))
    return render_template('chat.html', username = current_user.username, rooms = ROOMS )


# Logout Routing
@app.route('/logout', methods = ['GET'])
def logout():
    logout_user()
    flash("Logged out successfully", "Success")
    return redirect(url_for('login'))

#SocketIO Event Handling

@socketio.on('message')
def messages(data):
    print(data)
    send({'msg':data['msg'] ,'username': data['username_value'],
     'time_stamp': strftime('%b-%d %I:%M %p', localtime()), 'room_name': data['room']})
    #send(data)
    

@socketio.on('join')
def join(data):
    print('join', data)
    send({'msg': data['username'] + ' has joined ' + data['room'] + ' room.'} )

@socketio.on('leave')
def leave(data):
    print('leave',data)
    send({'msg': data['username'] + ' has left the ' + data['room'] + ' room.'})

if __name__ == "__main__": # __name__ will return the name of module that we are running
    #app.run(debug=True)
    socketio.run(app, debug = True)