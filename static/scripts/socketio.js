

document.addEventListener('DOMContentLoaded', () =>{

    const socket = io.connect('http://'+document.domain +':'+location.port);
    let room ='College Group';
    joinRoom(room);
        // socket.on('connect', ()=>{
        //     socket.send("I'm Connected!!" );
        // });

        // Disaplay Message 
        socket.on('message', data =>{
            
            const p = document.createElement('p');
            const br = document.createElement('br');
            const span_username = document.createElement('span');
            const span_time = document.createElement('span');
            console.log(data);
            if(data.username){
                span_time.innerHTML = data.time_stamp;
                span_username.innerHTML = data.username;
                p.innerHTML =span_username.outerHTML + br.outerHTML + data.msg + br.outerHTML + span_time.outerHTML;
                document.querySelector("#message_area").append(p);
                
                console.log(`Message Recieved: ${data.username}`);
            }else{
                printSysMsg(data.msg);
            }
        });

        document.querySelector("#send_msg").onclick = () => {

            socket.send({'msg':document.querySelector("#input_msg").value, 'username_value': username , 'room': room });
            // Clear Input Area 
            document.querySelector('#input_msg').value = '';
            document.querySelector('#input_msg').focus();
        }
        
        // Room Selection 

        document.querySelectorAll(".select-room").forEach(p =>{
            p.onclick = () =>{
                let newRoom = p.innerHTML;
                console.log(room);
                if(newRoom == room ){
                    msg = ` You are already in ${room} room.`;
                    printSysMsg(msg);
                }
                else{
                    // exit current room and join new room 
                    leaveRoom(room);
                    joinRoom(newRoom);
                    room = newRoom;
                }
            }
        });

        function printSysMsg(msg){
            const p = document.createElement('p');
            p.innerHTML = msg;
            document.querySelector('#message_area').append(p);
            document.querySelector('#input_msg').focus();
        }

        function leaveRoom(room){
            socket.emit('leave', {'room': room, 'username': username});
        }

        function joinRoom(newRoom){
            socket.emit('join', {'room':newRoom, 'username': username});
            // Clear the message area after user joined to new room 
            document.querySelector('#message_area').innerHTML = '';
            document.querySelector('#input_msg').focus();
        }

});